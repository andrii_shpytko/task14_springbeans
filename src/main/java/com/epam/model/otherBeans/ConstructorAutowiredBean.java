package com.epam.model.otherBeans;

import com.epam.model.beans1.BeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConstructorAutowiredBean {
    private OtherBeanA otherBeanA;
    private BeanA beanA;

    @Autowired
    public ConstructorAutowiredBean(OtherBeanA otherBeanA, BeanA beanA) {
        this.otherBeanA = otherBeanA;
        this.beanA = beanA;
    }

    @Override
    public String toString() {
        return "ConstructorAutowiredBean{" +
                "otherBeanA=" + otherBeanA +
                ", beanA=" + beanA +
                '}';
    }
}
