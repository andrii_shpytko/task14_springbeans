package com.epam.model.otherBeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanC {
    private String name;
    private int value;

    public OtherBeanC() {
    }

    public OtherBeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
