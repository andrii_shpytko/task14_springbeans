package com.epam.model.otherBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FieldAutowiredBean {
    @Autowired
    @Qualifier("otherB")
    private OtherBeanB otherBeanB;

    @Override
    public String toString() {
        return "FieldAutowiredBean{" +
                "otherBeanB=" + otherBeanB +
                '}';
    }
}
