package com.epam.model.otherBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SetterAutowiredBean {
    private OtherBeanC otherBeanC;

    public OtherBeanC getOtherBeanC() {
        return otherBeanC;
    }

    @Autowired
    public void setOtherBeanC(OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }

    @Override
    public String toString() {
        return "SetterAutowiredBean{" +
                "otherBeanC=" + otherBeanC +
                '}';
    }
}
