package com.epam.model.beans2;

import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
    private Color color;

    public NarcissusFlower() {
        color = Color.RED;
    }

    public NarcissusFlower(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "color=" + color +
                '}';
    }
}
