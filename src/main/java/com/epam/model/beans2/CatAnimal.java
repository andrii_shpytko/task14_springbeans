package com.epam.model.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    private String name;
    private int age;

    public CatAnimal() {
    }

    public CatAnimal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "CatAnimal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
