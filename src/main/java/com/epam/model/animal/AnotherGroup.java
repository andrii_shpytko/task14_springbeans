package com.epam.model.animal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class AnotherGroup {
    @Autowired
    @Qualifier("dog")
    private Animal dog;
    private Animal cat;

    public AnotherGroup() {
    }

    @Override
    public String toString() {
        return "AnotherGroup{" +
                "dog=" + dog +
                ", cat=" + cat +
                '}';
    }
}
