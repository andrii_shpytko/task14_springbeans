package com.epam.model.animal;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Cat implements Animal {
    @Override
    public String getAnimal() {
        return "Cat";
    }
}
