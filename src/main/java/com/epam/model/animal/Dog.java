package com.epam.model.animal;

import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal {
    private String type;

    public Dog() {
        this.type = "Dog";
    }

    @Override
    public String getAnimal() {
        return type;
    }
}
