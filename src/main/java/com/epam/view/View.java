package com.epam.view;

import com.epam.config.*;
import com.epam.model.animal.Animal;
import com.epam.model.animal.AnimalGroup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

import java.util.*;

public class View {
    private static final Logger LOGGER = LogManager.getLogger(View.class.getName());
    private static Scanner scan = new Scanner(System.in);

    private Map<String, String> menu;
    private Map<String, Runnable> menuMethods;
    private ResourceBundle resourceBundle;
    private ApplicationContext applicationContext;
    private AnnotationConfigApplicationContext context;

    public View() {
        resourceBundle = ResourceBundle.getBundle("Menu");
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        applicationContext = new AnnotationConfigApplicationContext(
                OtherBeansConfig.class, Beans1Config.class, FlowersConfig.class, Task8Config.class);
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scan.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("Q", resourceBundle.getString("Q"));

        menuMethods.clear();
        menuMethods.put("1", () -> showBeansName());
        menuMethods.put("2", () -> showAnimals());
        menuMethods.put("3", () -> showOthers());
        menuMethods.put("4", () -> checkMainProfile());
        menuMethods.put("5", () -> checkAdditionalProfile());
        outputMenu();
    }

    private void outputMenu() {
        LOGGER.trace("MENU:\n");
        for(String option: menu.values()){
            LOGGER.trace(option);
        }
    }

    private void checkAdditionalProfile() {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "additionalProfile");
        context = new AnnotationConfigApplicationContext(AdditionalProfileConfig.class);
        Arrays.stream(context.getBeanDefinitionNames()).forEach(LOGGER::info);
    }

    private void checkMainProfile() {
        context = new AnnotationConfigApplicationContext();
        context.getEnvironment();
        context.register(MainProfileConfig.class);
        context.refresh();
        Arrays.stream(context.getBeanDefinitionNames()).forEach(LOGGER::info);
    }

    private void showOthers() {
        LOGGER.info(applicationContext.getBean("otherBeanA").toString());
        LOGGER.info(applicationContext.getBean("otherBeanA").toString());
        LOGGER.info(applicationContext.getBean("otherBeanB").toString());
        LOGGER.info(applicationContext.getBean("otherBeanB").toString());
        LOGGER.info(applicationContext.getBean("otherBeanC").toString());
        LOGGER.info(applicationContext.getBean("otherBeanC").toString());
        LOGGER.info(((Animal) applicationContext.getBean("swallow")).getAnimal());
        LOGGER.info(((Animal) applicationContext.getBean("swallow")).getAnimal());
    }

    private void showAnimals() {
        AnimalGroup animals = (AnimalGroup) applicationContext.getBean("animalGroup");
        animals.getAnimals().forEach((Animal animal) -> LOGGER.info(animal.getAnimal()));
    }

    private void showBeansName() {
        for (String beanName : applicationContext.getBeanDefinitionNames()) {
            LOGGER.info(applicationContext.getBean(beanName).getClass().toString());
        }
    }
}
