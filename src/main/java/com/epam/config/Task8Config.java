package com.epam.config;

import com.epam.model.animal.AnotherGroup;
import com.epam.model.animal.Bird;
import com.epam.view.View;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.epam.model.animal")
public class Task8Config {
    private static final Logger LOGGER = LogManager.getLogger(View.class.getName());

    @Bean
    @Primary
    public AnotherGroup getBean() {
        return new AnotherGroup();
    }

    @Bean("swallow")
    @Scope("singleton")
    public Bird getAOtherBird() {
        return new Bird("swallow");
    }
}
