package com.epam.config;

import com.epam.model.beans1.BeanA;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans(
        {
                @ComponentScan("com.epam.model.otherBeans"),
                @ComponentScan(basePackages = "com.epam.model.beans1",
                        useDefaultFilters = false,
                        includeFilters = @ComponentScan.Filter(
                                type = FilterType.ASSIGNABLE_TYPE, classes = BeanA.class))
        }
)
public class OtherBeansConfig {
}
