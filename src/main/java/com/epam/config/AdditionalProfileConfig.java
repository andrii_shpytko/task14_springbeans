package com.epam.config;

import com.epam.model.animal.AnimalGroup;
import com.epam.model.animal.Cat;
import com.epam.model.beans2.NarcissusFlower;
import com.epam.model.otherBeans.FieldAutowiredBean;
import com.epam.model.otherBeans.OtherBeanB;
import org.springframework.context.annotation.*;

@Configuration
@Profile("additionalProfile")
@ComponentScans({
        @ComponentScan(basePackages = "com.epam.model.animal",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(
                        type = FilterType.ASSIGNABLE_TYPE,
                        classes = Cat.class)),
        @ComponentScan(basePackages = "com.epam.model.otherBeans",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(
                        type = FilterType.ASSIGNABLE_TYPE,
                        classes = OtherBeanB.class)),
})
public class AdditionalProfileConfig {
    @Bean
    public AnimalGroup getAnimalGroup() {
        return new AnimalGroup();
    }

    @Bean
    public NarcissusFlower getNarcissusFlower() {
        return new NarcissusFlower();
    }

    @Bean
    public FieldAutowiredBean getFieldBean() {
        return new FieldAutowiredBean();
    }
}
